### Домашнее задание к занятию "6.4. PostgreSQL"
## Задача 1
* вывода списка БД - \l
* подключения к БД - \с dbname ( где dbname имя БД)
* вывода списка таблиц - \dt
* вывода описания содержимого таблиц - \d+ tablename
* выхода из psql - \q

## Задача 2
test_database=# select MAX(avg_width) from pg_stats where tablename= 'orders';

test_database=# select avg_width, attname from pg_stats where tablename= 'orders';

## Задача 3

```
test_database=# CREATE TABLE orders_1 (
test_database(# CHECK ( price > 499 )
test_database(# ) INHERITS (orders);
CREATE TABLE

test_database=# CREATE TABLE orders_2 (
CHECK ( price < 499 )
) INHERITS (orders);
CREATE TABLE
```

## Задача 4

ALTER TABLE ONLY orders ADD UNIQUE (title);
